# automationpractice-serenity
This repository contains Serenity framework test scripts for http://automationpractice.com/ website.

### Including:
* [Pages package](https://gitlab.com/Liz-Lebedeva/automationpractice-selenium-java/tree/master/src/test/java/pages):
Page classes based on _Page Object Model_ design pattern
* [Steps package](https://gitlab.com/Liz-Lebedeva/automationpractice-serenity/tree/master/src/test/java/steps) and 
[Features package](https://gitlab.com/Liz-Lebedeva/automationpractice-selenium-java/tree/master/src/test/java/scripts):
Test Scripts. See Serenity documentation for the additional information.
* [Helper package](https://gitlab.com/Liz-Lebedeva/automationpractice-selenium-java/tree/master/src/test/java/helper): 
Helper classes (list of page urls, test data generation etc)
* [Amazing test reports](https://gitlab.com/Liz-Lebedeva/automationpractice-serenity/tree/master/target/site/serenity). 
To see them in html, click [here](https://rawgit.com/Liz-Lebedeva/automationpractice-serenity/master/target/site/serenity/index.html)

### Built With:
Scripts are written in Java and require [Serenity](http://www.thucydides.info/docs/serenity/) framework and [Gradle](https://gradle.org) for the dependency management. 

