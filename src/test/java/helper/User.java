package helper;

import java.time.LocalDate;


public class User {
    
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private LocalDate dob;
    
    
    public User() {
	// only these attributes (email, password, first name, last name and date of birth) 
	// are stored in the website's database, so i do not generate anything for other fields
	this.email = generate_email();
	this.password = generate_password();
	this.firstName = "Test";
	this.lastName = "User";
	this.dob = generate_dob();
    }
    

    /* Getters */
    
    public String get_email() {
	return this.email;
    }

    public String get_password() {
	return this.password;
    }

    public String get_first_name() {
	return this.firstName;
    }

    public String get_last_name() {
	return this.lastName;
    }

    public int get_year_of_birth() {
	return this.dob.getYear();
    }

    public int get_month_of_birth() {
	return this.dob.getMonthValue();
    }

    public int get_day_of_birth() {
	return this.dob.getDayOfMonth();
    }


    /* Setters */

    public User set_email(String in) {
	this.email = in;
	return this;
    }

    public User set_password(String in) {
	this.password = in;
	return this;
    }

    public User set_first_name(String in) {
	this.firstName = in;
	return this;
    }

    public User set_last_name(String in) {
	this.lastName = in;
	return this;
    }

    public User set_date_of_birth(int yyyy, int mm, int dd) {
	this.dob = LocalDate.of(yyyy, mm, dd);
	return this;
    }

    
    /* Helper methods */
    
    private int generate_num(int min, int max) {
	return (int) (Math.random() * (max - min) + min);
    }

    private String generate_email() {
	// adds random suffix to @gmail.com email address to generate unique email address for each new user
	// (characters used: upper - no, lower - yes, digits - yes, special symbols - no)
	return "my.precious.test.user+" + StringGenerator.generate(false, true, true, false, 3, 8) + "@gmail.com";		
    }

    private String generate_password() {
	// generate random string of 5 to 15 characters length
	return StringGenerator.generate(5,15);		
    }
    
    private LocalDate generate_dob() {
	// generates date of birth between 1950 and 2000
	int startDate = (int) LocalDate.of(1950, 1, 1).toEpochDay();
	int endDate = (int) LocalDate.of(1999, 12, 31).toEpochDay();
	
	return LocalDate.ofEpochDay(generate_num(startDate, endDate));		
    }
    
    
}
