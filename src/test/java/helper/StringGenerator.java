package helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StringGenerator {

    private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String DIGITS = "0123456789";
    private static final String SPECIALS = "!@#$%&*()_+-=[]|,./?><";


    public static String generate(boolean useUpper, boolean useLower, boolean useDigits, boolean useSpecials, 
	    				int minlength, int maxlength) {
	
	int length = (int) (Math.random() * (maxlength - minlength) + minlength);
	
        StringBuilder result = new StringBuilder(length);
        Random random = new Random(System.nanoTime());

        // collect required categories
        List<String> charCategories = new ArrayList<>(4);
        if (useLower) {
            charCategories.add(LOWER);
        }
        if (useUpper) {
            charCategories.add(UPPER);
        }
        if (useDigits) {
            charCategories.add(DIGITS);
        }
        if (useSpecials) {
            charCategories.add(SPECIALS);
        }

        // build string
        for (int i = 0; i < length; i++) {
            String charCategory = charCategories.get(random.nextInt(charCategories.size()));
            int position = random.nextInt(charCategory.length());
            result.append(charCategory.charAt(position));
        }
        return new String(result);	
    }

    // short version of the method above in case when no length variety is required
    public static String generate(boolean useUpper, boolean useLower, boolean useDigits, boolean useSpecials, 
		int length) {
	return generate(useUpper, useLower, useDigits, useSpecials, length, length);	
    }

    
    // short version of the method above in case when all 4 categories are required
    public static String generate(int minlength, int maxlength) {
	return generate(true, true, true, true, minlength, maxlength);	
    }
}
