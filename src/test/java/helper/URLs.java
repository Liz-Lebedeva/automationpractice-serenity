package helper;

public final class URLs {

    public static final String BASE_URL = "http://automationpractice.com";
    
    public static final String AUTH_PAGE = BASE_URL + "/index.php?controller=authentication&back=my-account";
    public static final String AUTH_PAGE_STEP2 = BASE_URL + "/index.php?controller=authentication&back=my-account#account-creation";
}
