package steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsNot.*;
import static org.hamcrest.core.StringContains.*;
import static org.hamcrest.core.StringStartsWith.*;

import pages.AuthenticationPage;
import helper.URLs;
import helper.User;

public class CreateAccountSteps extends ScenarioSteps {

    AuthenticationPage authenticationPage;
    
    //String link = new String();
    
    
    /* 1st Step of Registration */
    
    @Step("GIVEN user opens \'...\' page")
    public void user_opens_auth_page() {
	authenticationPage.open();
	assertThat("\'...\' page did not open", authenticationPage.getCurrentURL(), is(URLs.AUTH_PAGE) );
    }

    
    @Step("WHEN user enters email address as \'{0}\'")
    public void user_enters_email(String email) {
	authenticationPage.create_new_account(email);

    }

    @Step("AND clicks on \'create an account\'")
    public void user_submits_1st_step() {
	authenticationPage.sign_up(true);

    }

    @Step("THEN user lands on \'...\' page")
    public void user_transferred_to_2_step() {
	assertThat("\'Complete Registration\' page did not open", authenticationPage.getCurrentURL(), is(URLs.AUTH_PAGE_STEP2) ); 
    }


    
}
