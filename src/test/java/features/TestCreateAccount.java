package features;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import steps.CreateAccountSteps;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import helper.User;


@RunWith(SerenityRunner.class)
public class TestCreateAccount {
    
    public static User user;

    @Managed
    public static WebDriver browser;
    
    @Steps
    public CreateAccountSteps steps;

    
//  @Ignore
    @Test @Title("User completes 1st step of registration")
    public void registration_1_step() {	
	user = new User();
	
	steps.user_opens_auth_page();
	steps.user_enters_email(user.get_email());
	steps.user_submits_1st_step();
	steps.user_transferred_to_2_step();	
    }
    
    @Pending
    @Test @Title("User completes 2nd step of registration")
    public void registration_2_step() {		
		
    }    
    
}
