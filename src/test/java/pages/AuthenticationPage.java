package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.URLs;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl(URLs.AUTH_PAGE)
public class AuthenticationPage extends PageSkeleton {

    // TODO
    public AuthenticationPage(WebDriver driver) {
        super(driver, URLs.AUTH_PAGE); 
    }

    
    /* Login Selectors */
    
    @FindBy(css = "input[id='email']")
    private WebElement login_input_email;

    @FindBy(css = "input[id='passwd']")
    private WebElement login_input_password;
    
    @FindBy(css = "button[id='SubmitLogin']")
    private WebElement login_submit;       
    
    /* Create New Account Selectors */
    
    @FindBy(css = "input[id='email_create']")
    private WebElement create_input_email;

    @FindBy(css = "button[id='SubmitCreate']")
    private WebElement create_submit;
    
    /* Alerts Selectors */
    
    @FindBy(className = "alert-danger")
    private WebElement alert;
    

    /* Methods */

    // TODO
/*    public AuthenticationPage open_page() {
	open_page();
	return this;
    }
 */
    
    public String get_alert_message() {
	if ( alert.isDisplayed() ) {
	    return alert.getText();
	}
	return null;
    }
    
    
    /* Methods for Login Feature */
    
    public AuthenticationPage enter_user_email(String keys) {
	enter_text(login_input_email, keys);
	return this;
    }
    
    public AuthenticationPage enter_user_password(String keys) {
	enter_text(login_input_password, keys);
	return this;
    }
    
    public void log_in(boolean expectSuccess) {
	submit(login_submit, expectSuccess);	
    }
    
    
    /* Methods for Create New Account Feature */

    public AuthenticationPage create_new_account(String keys) {
	enter_text(create_input_email, keys);
	return this;
    }

    public void sign_up(boolean expectSuccess) {
	submit(create_submit, expectSuccess);	
    }
       
    
}
