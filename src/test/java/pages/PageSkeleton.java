package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.pages.PageObject;

public class PageSkeleton extends PageObject {
    
    private WebDriver driver;    
    private String baseURL;    
    
// TODO
    public PageSkeleton(WebDriver driver, String baseURL) {
        this.driver = driver;
        this.baseURL = baseURL;
    }


    /* Selectors */
    
    @FindBy(className = "alert")
    private WebElement alert;
    
    
    /* Methods */

    // TODO
/*    protected void open_page() {
	driver.get(baseURL);
	
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until( ExpectedConditions.and(
		ExpectedConditions.urlToBe(baseURL),
		ExpectedConditions.presenceOfElementLocated(By.id("page"))		
		) );

    }
*/

    public String getCurrentURL() {
	return driver.getCurrentUrl();
    }
    
    protected void enter_text(WebElement element, String keys) {
	WebDriverWait wait = new WebDriverWait(driver, 3);
	wait.until( ExpectedConditions.visibilityOf(element) );

	element.click();
	element.clear();
	element.sendKeys(keys);	
    }

    protected void check(WebElement element) {	
	WebDriverWait wait = new WebDriverWait(driver, 3);
	wait.until( ExpectedConditions.visibilityOf(element) );

	if ( ! element.isSelected() ) {
	    element.click();
	}
    }

    protected void uncheck(WebElement element) {	
	WebDriverWait wait = new WebDriverWait(driver, 3);
	wait.until( ExpectedConditions.visibilityOf(element) );
	
	if ( element.isSelected() ) {
	    element.click();
	}
    }

    // 'submit' method can be used both in valid (expectSuccess = true) and invalid (expectSuccess = false) test cases
    protected void submit(WebElement element, boolean expectSuccess) {
	
	element.click();
	
	if (expectSuccess) {
	    WebDriverWait wait = new WebDriverWait(driver, 30);	
	    
	    // wait until current url will differ from 'baseURL' (which means that user landed on any other page)
	    wait.until( new ExpectedCondition<Boolean>(){		
		public Boolean apply(WebDriver driver){
	    		return ( ! driver.getCurrentUrl().equalsIgnoreCase(baseURL) );
		}
	    });	
	} else {
	    
	    //wait until any alert will be displayed
	    WebDriverWait wait = new WebDriverWait(driver, 30);
	    wait.until( ExpectedConditions.visibilityOf(alert) );    
	}	
    }

}
